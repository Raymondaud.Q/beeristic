class Vehicle_Manager{
  
  constructor(rate=0.5){
    this._vehicles = [];
    this.finishRate = rate;
    this.finishedd = false;
    this.artificialFinishChrono = new Chrono();
    this.artificialFinishChrono.start()
  }
  
  size(){
    return this._vehicles.length;
  }

  add ( vehicle ) {
    this._vehicles.push(vehicle);
  }
  
  getVehicles(){
    return this._vehicles ;
  }
  
  clear(){
    Basic_Vehicle.num = -1;
    for ( let vehicle of this._vehicles )
      vehicle.remove();
    //this.vehicles = [];
  }
  
  getTimes(){
    return this._vehicles.map(a =>  { 
      return{ index: a.index,
              times: a.times };
    }); 
  }
  
  getAIDatas(){
    return this._vehicles.map(a =>  { 
      return a.getAIDatas() ;
    }); 
  }

  show(nb)
  {
    let cnt = 0 ;
    for ( let vehicle of this._vehicles ){
      vehicle.show( ( cnt < (nb * this._vehicles.length) / 100) );
      cnt ++;
    }
  }
  
  isFinished(){
    return this.finishedd;
  }

  restartEndChrono(){
    this.artificialFinishChrono.start();
  }
  

  // TODO : vehicle stuck (not killed & not finished)
  finished(){
    if ( this._vehicles.length == 0)
      this.finishedd = true

    let nbFinishedNotKilled = 0;
    let nbKilled = 0;
    if ( Basic_Gate.num == -1 ) return (this.finishedd = false);
    for ( let vehicle of this._vehicles ){
      if (vehicle.killed )
        nbKilled += 1
      if ( vehicle.finished() && !vehicle.killed){
        nbFinishedNotKilled ++;
        if ( nbFinishedNotKilled  >= this.size() * this.finishRate )
          return (this.finishedd = true);
      } else if ( nbFinishedNotKilled + nbKilled < this.size() ){
          if ( this.artificialFinishChrono.tour() > 10000){
            vehicle.finish()
          }
      }

    }

    if ( nbKilled == this._vehicles.length || nbFinishedNotKilled + nbKilled == this._vehicles.length)
       return (this.finishedd = true);

    return false
  }

}
