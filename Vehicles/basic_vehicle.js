class Basic_Vehicle {
  static num = -1;
  constructor(map, x, y, d, ia) {
    this._body = Bodies.circle(x, y, d/2, {
      density: 1,
      friction: 1,
      frictionStatic: 5,
      diameter: d,
      collisionFilter: { category: vehicle,
                          mask: cgate | gate | kgate  }
    });
    World.add(world, this._body);
    this._body.type="Vehicle";
    this._body.vehicle = this;
    this._agent = ia;
    this._agent.setVehicle(this);
    this._toCheck = 0;
    this._map = maps;
    this.index = ++Basic_Vehicle.num;
    this.times = [];
    this.times.push(-1);
    this.killed = false;
  }


  remove(){
    World.remove(world, this._body);
    delete this.times;
    delete this.index;
    delete this._map;
    delete this._body;
    delete this._agent;
    delete this._toCheck;
  }
  
  getPosition() {
    //console.log(this._body);
    if ( this._body ){
      const pos = this._body.position;
      return { x:pos.x, y:pos.y, r:this._body.diameter/2 };
    }
  }

  getToCheck() {
    return this._toCheck;
  }

  getTime(index = this.times.length-1 ){
    return this.times[index];
  }

  setTime(time){
    this.times[this.times.length-1] = time;
    this.times.push(-1);
    this._toCheck ++;
  }

  getAIDatas(){
    return this._agent.getDatas();
  }

  finish(){
    let index = -1
    for ( let i = 0 ; i <= Basic_Gate.num ; i ++ ){
       if ( this.times[i] == -1) {
        index = i
        break;
       }
    }
    if ( index !== -1){
      for (let i = index; i <= Basic_Gate.num; i++){
        this.setTime(999999 - index*100)
      }
    }
    this.killed = true;
  }


  finished(){
    if ( Basic_Gate.num == -1 ) 
      return false;
    for ( let i = 0 ; i <= Basic_Gate.num ; i ++ ){
      if ( this.times[i] == -1) 
        return false;
    }
    return true;
  }

  show(display) {
    if ( this._agent.can("applyForce") && this._map.isStarted() ){
      const forces = this._agent.applyForce();
      if ( forces != undefined ){
        Matter.Body.applyForce(this._body, this._body.position,forces);
        Matter.Body.setAngle(this._body, forces.ta-3.14/2);
      }
    }
    const pos = this._body.position;
    const angle = this._body.angle;
    push();
    translate(pos.x, pos.y);
    rotate(angle);
    if ( display ){
      fill(255);
      imageMode(CENTER);
      image(dotImg, 0, 0, this._body.diameter, this._body.diameter);
    }
    pop();
  }
}