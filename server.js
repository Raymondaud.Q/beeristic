const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
	res.sendFile('index.html', {root: __dirname })
})
app.get('/sketch', (req,res) => { 
	res.sendFile('sketch.js', {root: __dirname})
})

app.use('/img', express.static('drawings'))
app.use('/AI', express.static('AI'))
app.use('/Gates', express.static('Gates'))
app.use('/Engine', express.static('Engine'))
app.use('/Vehicles', express.static('Vehicles'))
app.use('/lib', express.static('lib'))
app.use('/style', express.static('style'))

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
})
