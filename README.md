# Heuristic Bees Path Finding in JavaScript

Path finding on 2D physics based environment using heuristics
[**Here you can try Beeristic**](https://quentinraymondaud.itch.io/beeristic)
![Presentation](https://gitlab.com/Raymondaud.Q/beeristic/-/raw/master/bees.gif)

## Dependencies

- [P5js](https://p5js.org/)
- [MatterJs](https://brm.io/matter-js/)
- [ExpressJs](https://expressjs.com/)
- [NodeJS & NPM](https://nodejs.org/en/download/package-manager/)

## Dependencies that you must install

- [NodeJS & NPM](https://nodejs.org/en/download/package-manager/)

## Build & Run the projet :

- Download this rep
- Extract Files
- Run `npm i`
- Run `node server.js`
- Go to localhost:3000