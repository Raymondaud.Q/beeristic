class PostProcess{
  
  constructor(infos, iaDatas, selectionRate ){
    this.datas = iaDatas;
    this.infos = infos;
    this._size = iaDatas.length;
    this._selectionRate = selectionRate;
    this.process(this.minTime())
  }
  
  process(lan){
    this.bestTimes(lan);
    var j = 0;
    var rand = 0;
    while (j < this._size ) {
      for ( let i of lan ) {
        rand = Math.random();
        if ( this.datas != undefined && this.datas[i] != undefined ){
          this.datas[j] =  JSON.parse(JSON.stringify(this.datas[i]));
          if ( j > lan.length*0.25 ){
            for ( let m = 0 ; m < this.datas[i].length ; m ++ ){
              rand = Math.random();
              if ( rand < 0.15 ){
                for ( let mm = m ; mm < this.datas[i].length ; mm ++)
                  this.datas[j][mm] = undefined
                break
              } else {
                rand = Math.random()
                if (rand < 0.2 && this.datas[j][m] != null ){
                  this.datas[j][m].fx += randn_bm(-0.05, 0.05, rand)
                  this.datas[j][m].fy += randn_bm(-0.05, 0.05, rand)
                  this.datas[j][m].targetAngle = Matter.Vector.angle(this.datas[j][m].currentPos,
                                                                     this.datas[j][m].nextTarget) + randn_bm(-10, 10, 1);
                } else if ( rand >= 0.2 && rand < 0.3 && this.datas[j][m] != null ) {
                    this.datas[j][m].fx += randn_bm(-0.25, 0.25, rand)
                    this.datas[j][m].fy += randn_bm(-0.25, 0.25, rand)
                    this.datas[j][m].targetAngle = Matter.Vector.angle(this.datas[j][m].currentPos,
                                                                       this.datas[j][m].nextTarget) + randn_bm(-10, 10, 1);
                }  else if ( rand >= 0.3 && rand < 0.4 && this.datas[j][m] != null) {
                    this.datas[j][m].fx += randn_bm(-0.5, 0.5, rand)
                    this.datas[j][m].fy += randn_bm(-0.5, 0.5, rand)
                    this.datas[j][m].targetAngle = Matter.Vector.angle(this.datas[j][m].currentPos,
                                                                       this.datas[j][m].nextTarget) + randn_bm(-10, 10, 1);
                } else if ( rand >= 0.4 && rand < 0.5 && this.datas[j][m] != null) {
                    this.datas[j][m].fx += randn_bm(-1, 1, rand)
                    this.datas[j][m].fy += randn_bm(-1, 1, rand)
                    this.datas[j][m].targetAngle = Matter.Vector.angle(this.datas[j][m].currentPos,
                                                                       this.datas[j][m].nextTarget) + randn_bm(-10, 10, 1);
                } else if ( rand >= 0.5 && rand < 0.6 && this.datas[j][m] != null) {
                    this.datas[j][m].fx += randn_bm(-2, 2, rand)
                    this.datas[j][m].fy += randn_bm(-2, 2, rand)
                    this.datas[j][m].targetAngle = Matter.Vector.angle(this.datas[j][m].currentPos,
                                                                       this.datas[j][m].nextTarget) + randn_bm(-10, 10, 1);
                } else if ( rand >= 0.7 && rand < 0.8 && this.datas[j][m] != null) {
                    this.datas[j][m].fx += randn_bm(-5, 5, rand)
                    this.datas[j][m].fy += randn_bm(-5, 5, rand)
                    this.datas[j][m].targetAngle = Matter.Vector.angle(this.datas[j][m].currentPos,
                                                                       this.datas[j][m].nextTarget) + randn_bm(-10, 10, 1);
                } else if ( rand >= 0.8 && rand < 0.9 && this.datas[j][m] != null) {
                    this.datas[j][m].fx += randn_bm(-10, 0, rand)
                    this.datas[j][m].fy += randn_bm(-10, 0, rand)
                    this.datas[j][m].targetAngle = Matter.Vector.angle(this.datas[j][m].currentPos,
                                                                       this.datas[j][m].nextTarget) + randn_bm(-10, 10, 1);
                } else if ( this.datas[j][m] != null) {
                    this.datas[j][m].fx += randn_bm(0, 10, rand)
                    this.datas[j][m].fy += randn_bm(0, 10, rand)
                    this.datas[j][m].targetAngle = Matter.Vector.angle(this.datas[j][m].currentPos,
                                                                       this.datas[j][m].nextTarget);
                }  
              }               
            }
          }
        }
        j++
      }
    }
  }
  
  minTime(){
    var indexList = [];
    let iMin = -1;
    while ( indexList.length < this._size * this._selectionRate ){
      let min = Math.min();
      for ( let i = 0 ; i < this._size ; i ++ ){
        if ( !(indexList.includes(i)) &&
              this.infos[i].times[this.infos[i].times.length-2] <= min &&
              this.infos[i].times.length-1 == Basic_AI.gatesPos.length ){
          min = this.infos[i].times[this.infos[i].times.length-2] ;
          iMin = i;
        }
      }
      if (!(indexList.includes(iMin))) 
        indexList.push(iMin);
    }
    return indexList;
  }
  
  bestTimes(lan){
    let cnt = 0 
    for ( let i of lan ) {
      if (cnt <= 5){
        if ( cnt == 0 )
          globalMinTime = this.infos[i].times[this.infos[i].times.length-2]
        console.log("-N°" + i + " takes " + this.infos[i].times[this.infos[i].times.length-2] + " ms");
        cnt += 1
      }
      else {
        console.log("Next Generation")
        break;
      }
    }
  }
  
}