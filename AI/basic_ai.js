class Basic_AI {

  static gatesPos;
  constructor(args = undefined) {
    this._index = 0 ;
    if ( args != undefined )
      this._datas = args;
    else 
      this._datas = [];
  }

  setVehicle(ve) {
    this._vehicle = ve;
  }
  
  getDatas(){
    return this._datas;
  }

  applyForce() {

    const indexG = this._vehicle.getToCheck();
    var nextTarget = { x:0 , y:0 , r:0 };
    if (indexG < Basic_AI.gatesPos.length){
      var forceX ;
      var forceY ;
      var targetAngle ;
      const currentPos = this._vehicle.getPosition();
      if ( this._datas[this._index] == undefined ){

        let rand = Math.random()
        if ( rand < 0.2 ){
          forceX = randn_bm(-0.005, 0.005, rand) ;
          forceY = randn_bm(-0.005, 0.005, rand) ;
        } else if (rand > 0.2 && rand < 0.5){
          forceX = randn_bm(-0.015, 0.015, rand) ;
          forceY = randn_bm(-0.015, 0.015, rand) ;
        } else if (rand > 0.4 && rand < 0.6){
          forceX = randn_bm(-0.05, 0.05, rand) ;
          forceY = randn_bm(-0.05, 0.05, rand) ;
        } else if (rand > 0.6 && rand < 0.75){
          forceX = randn_bm(-0.5, 0, rand) ;
          forceY = randn_bm(-0.5, 0, rand) ;
        } else {
          forceX = randn_bm(0, 0.5, rand) ;
          forceY = randn_bm(0, 0.5, rand) ;
        }

        nextTarget = JSON.parse(JSON.stringify( Basic_AI.gatesPos[indexG]));
        targetAngle = Matter.Vector.angle(currentPos, nextTarget);
        this._datas[this._index]={ currentPos: currentPos, nextTarget: nextTarget, targetAngle: targetAngle, fx: forceX, fy: forceY};
        this._index++;
      }
      else {
        //console.log(this._index);
        forceX = this._datas[this._index].fx;
        forceY = this._datas[this._index].fy;
        targetAngle = this._datas[this._index].targetAngle;
        //targetAngle = Matter.Vector.angle(currentPos, this._datas[this._index].nextTarget);
        this._index++;
      }
      return {
        x: cos(targetAngle) * forceX,
        y: sin(targetAngle) * forceY,
        ta: targetAngle
      };
    }
  }

  can(methodName) {
    //console.log((typeof this[methodName]) == "function");
    return ((typeof this[methodName]) == "function");
  }

}