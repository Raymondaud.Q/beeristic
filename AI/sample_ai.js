class Sample_AI extends Basic_AI {
  
  constructor(args = undefined) {
    super(args);
  }
  
  applyForce() {

    const index = this._vehicle.getToCheck();
    var nextTarget = { x:0 , y:0 , r:0 };
    if (index != Basic_AI.gatesPos.length){
      var forceX ;
      var forceY ;
      var targetAngle ;
      const currentPos = this._vehicle.getPosition();
        if ( Math.random() > 0.4 ){
          forceX = (Math.random()-0.4)/2 ;
          forceY = (Math.random()-0.4)/2 ;
        }
        else{
          forceX = (Math.random()-0.3) ;
          forceY = (Math.random()-0.3) ;
        }
        nextTarget = JSON.parse(JSON.stringify( Basic_AI.gatesPos[index]));
        targetAngle = Matter.Vector.angle(currentPos, nextTarget);
        this._index ++;
       return {
        x: cos(targetAngle) * forceX,
        y: sin(targetAngle) * forceY,
        ta: targetAngle
      };
    }
  }

}