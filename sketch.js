

var Engine = Matter.Engine,
  World = Matter.World,
  Bodies = Matter.Bodies,
  Events = Matter.Events,
  Composite = Matter.Composite;

var engine = Engine.create(),
    world = engine.world;
    let runner = Matter.Runner.run(engine)

let globalMinTime = -1
let maps;
let vehicleMaxStuckTime = 10000 //10 sec without validating checkpoint => KillBee
let button;
let btnMore;
let btnLess;
let btnGateType;
let gateTypeIndex = 0;
let gateTypes = ["Gate", "CGate", "KGate"]
let slider;
let dotImg;
let gateImg;
let gateSize = 50
let vehicleSize = 30
var mapLoaded = false;
let gate = 0x0001,
  cgate = 0x0002,
  kgate = 0x0003,
  vehicle = 0x0004;

let NB_VEHICLES = 50; // This is how much bee will fly each round
const FINISH_Rate = 0.2 // 20% best of them should have finished to skip to next gen
const SELECTION_Rate = 0.2   // 20% of the best genes would be conserved


function preload() {
  dotImg = loadImage('img/dessin.png');
  gateImg = loadImage('img/fleur.png');
}

function randn_bm(min, max, skew) {
  let u = 0, v = 0;
  while(u === 0) u = Math.random() //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random()
  let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v )
  
  num = num / 10.0 + 0.5 // Translate to 0 -> 1
  if (num > 1 || num < 0) 
    num = randn_bm(min, max, skew) // resample between 0 and 1 if out of range
  
  else{
    num = Math.pow(num, skew) // Skew
    num *= max - min // Stretch to fill range
    num += min // offset to min
  }
  return num
}

function switchGateType(){
  gateTypeIndex = (gateTypeIndex + 1) % gateTypes.length
  btnGateType.elt.textContent = "Gate Type : " + gateTypes[gateTypeIndex]
}

function setup() {

  maps = new AIMap(screen.width, screen.height, FINISH_Rate, SELECTION_Rate);
  let canvas = createCanvas(window.innerWidth, window.innerHeight);
  const mouse = Matter.Mouse.create(canvas.elt);
  mouse.pixelRatio = pixelDensity();
  mConstraint = Matter.MouseConstraint.create(engine, {
    mouse: mouse
  });
  World.add(world, mConstraint)
  engine.world.gravity.y = 0;
  slider = createSlider(1, 100, 10);
  slider.position(25, 115);
  slider.style('width', '80px');

  button = createButton('GO !');
  button.mousePressed(start);
  button.position(15, 10);
  button.size(50, 20);


  btnLess = createButton('-10');
  btnLess.mousePressed(()=>{if (!maps.isStarted())NB_VEHICLES-=10});
  btnLess.position(70, 10);
  btnLess.size(30, 20);


  btnMore = createButton('+10');
  btnMore.mousePressed(()=>{if (!maps.isStarted()) NB_VEHICLES+=10});
  btnMore.position(105, 10);
  btnMore.size(30, 20);

  btnLess = createButton('Save');
  btnLess.mousePressed(download);
  btnLess.position(15, 45);
  btnLess.size(60, 20);

  btnLess = createFileInput(handleFile);
  btnLess.position(75, 45);
  btnLess.size(60, 20);

  btnGateType = createButton("Gate Type : " + gateTypes[gateTypeIndex]);
  btnGateType.mousePressed(switchGateType);
  btnGateType.position(15, 70);
  btnGateType.size(125, 20);
  }

function handleFile(files) {
  var file = files.file;
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.onload = function(e) {
    var data = JSON.parse(e.target.result)
    maps.clearSpawn()
    console.log(data)
    for (let gate of data.gatePos){
      maps.addGate(new Basic_Gate(gate.x, gate.y, gate.r));
    }
    for (let obs of data.obstaclePos ){
      if (obs.t == "CGate")
        maps.addGate(new Collidable_Gate(obs.x,  obs.y, obs.r));
      else if (obs.t == "KGate")
        maps.addGate(new Kill_Gate(obs.x,  obs.y, obs.r));
    }

    maps.setSpawn(data.spawnPos.x, data.spawnPos.y)
    NB_VEHICLES = data.ai.length
    for (let gen of data.ai ){
      maps.addVehicle(new Basic_Vehicle(maps, data.spawnPos.x, data.spawnPos.y, vehicleSize, new Basic_AI(gen)));
    }
    button.elt.textContent="GO !"
  };
  reader.readAsText(file);
}

function download() {
  var file = new Blob([JSON.stringify(
    { "spawnPos": maps.getSpawn(),
      "gatePos":maps.getGatesPos(),
      "obstaclePos": maps.getObstaclesPos(),
      "ai":maps.getAI()})]
  ,{type: "json"});

  let filename = "beeristicSave.json"
  if (window.navigator.msSaveOrOpenBlob) // IE10+
    window.navigator.msSaveOrOpenBlob(file, filename);
  else { // Others
    var a = document.createElement("a"),
      url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);  
    }, 0); 
  }
}

function start() {
  if (!maps.isStarted() && !!maps.getSpawn()) {
    if (!mapLoaded){
      for (let i=0; i < NB_VEHICLES; i++)
        maps.addVehicle(new Basic_Vehicle(maps, maps.getSpawn().x, maps.getSpawn().y, vehicleSize, new Basic_AI()));
    }
    maps.start();
    button.elt.textContent="STOP !"
    mapLoaded = false
  } else {
    maps.clearSpawn();
    button.elt.textContent="GO !"
  }
}


function createGateFromTypeAndPosition(gateTypeIndex, x, y, size){
  if (gateTypes[gateTypeIndex] == "Gate"){
    return new Basic_Gate(x, y, size)
  } else if (gateTypes[gateTypeIndex] == "CGate"){
    return new Collidable_Gate(x, y, size)
  } else if (gateTypes[gateTypeIndex] == "KGate"){
    return new Kill_Gate(x, y, size)
  }
}

function mouseClicked() {
  if (!maps.isStarted() && mouseX > 150 ){
    if(maps.getSpawn() == undefined){
      maps.setSpawn(mouseX,mouseY)
    } else {
      maps.addGate(createGateFromTypeAndPosition(gateTypeIndex, mouseX, mouseY, gateSize));
    } 
  }
}

function mouseDragged() {
  mouseClicked()
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function hexagon(x, y, r) {
  beginShape();
  for (let a = 0; a < 2 * PI; a += 2 * PI / 6) {
    let x2 = cos(a) * r;
    let y2 = sin(a) * r;
    vertex(x + x2, y + y2);
  }
  endShape(CLOSE);
}

function draw() {
  background("#003300");
  fill(123)
  rect(0,0,150,screen.height)
  fill(0)
  text("Show " + str((slider.value() * NB_VEHICLES) / 100)  + " fitest bees",15 ,105)
  text("Best Time : " + globalMinTime + " ms",15 ,155)

  text( str(NB_VEHICLES) +" Bees", 50, 42)
  fill(0)
  frameRate(60)

  if (maps.isFinished() && maps._vehicle_manager._vehicles.length > 0 )
    maps.postProcess();
  else {
    if(!!maps.getSpawn())
      hexagon(maps.getSpawn().x, maps.getSpawn().y, 20);
    let nbShow = slider.value();
    maps.nbShow = nbShow;
    maps.run();
  }
}