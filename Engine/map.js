class AIMap {

  constructor(width, height, finishR, selectionR) {
    Basic_Gate.num = Basic_Vehicle.num = -1;
    this.width = width;
    this.height = height;
    this.finishRate = finishR
    this.selectionRate = selectionR;
    this._vehicle_manager = new Vehicle_Manager(this.finishRate);
    this._gate_manager = new Gate_Manager();
    this._obstacles = []
    this._logic = new Basic_Rules();
    this._start = false;
    this.nbShow = 1 ;
    this.datas = undefined;
  }

  restartEndChrono(){
    this._vehicle_manager.restartEndChrono();
  }

  addVehicle(vehicle) {
    this._vehicle_manager.add(vehicle);
  }

  addGate(gate) {
    if (gate._body.type === "Gate")
      this._gate_manager.add(gate);
    else
      this._obstacles.push(gate)
  }

  setSpawn(x,y){
    this.spawnPos = {"x":x, "y":y}
  }

  getSpawn(){
    return this.spawnPos;
  }

  isStarted() {
    return this._start;
  }

  isFinished() {
    return this._finished;
  }

  start() {
    if (!this._start) {
      this._start = true;
      this._finished = false;
      Basic_AI.gatesPos = this._gate_manager.getGatesPos();
      this._logic.start();
    } 
  }

  clear() {
    World.clear(world);
    this._start = false;
    this._vehicle_manager.clear();
    this._gate_manager.clear();
    this._obstacles = []

  }

  clearSpawn(){
    this.clear()
    this._vehicle_manager = new Vehicle_Manager(this.finishRate);
    this.spawnPos = undefined;
    this._finished = false;
    this._logic = new Basic_Rules();
  }

  genVehicles(){
    World.clear(world, true);
    this._start= false;
    let nb = this._vehicle_manager.size()
    this._vehicle_manager.clear();
    this._vehicle_manager = new Vehicle_Manager(this.finishRate);
    this._gate_manager.start();
    for (let i = 0; i < nb ; i++){
      this.addVehicle(new Basic_Vehicle(
        this,
        this.getSpawn().x,
        this.getSpawn().y,
        vehicleSize,
        new Basic_AI(this._postProcess.datas[i])
      ));
    }
    this.start();
  }

  getAI(){
    return JSON.parse(JSON.stringify(this._vehicle_manager.getAIDatas()))
  }


  getGatesPos(){
    return JSON.parse(JSON.stringify(this._gate_manager.getGatesPos()))
  }

  getObstaclesPos(){
    let pos = [];
    for ( let obstacle  of this._obstacles)
      pos.push(obstacle.getPosition());
    return JSON.parse(JSON.stringify(pos))
  }

  postProcess(){
    if (this.isStarted())
      this._postProcess = new PostProcess( this._vehicle_manager.getTimes(), this._vehicle_manager.getAIDatas(), this.selectionRate); 
    this.genVehicles();
  }
  
  run() {
    this._finished = this._vehicle_manager.finished();
    this._gate_manager.show();  
    this._vehicle_manager.show(this.nbShow);
    for (let obstacle of this._obstacles){
      obstacle.show()
    }
  }
}