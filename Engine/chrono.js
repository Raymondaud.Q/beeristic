class Chrono{
  
  constructor() {
    var startTime, inTime, endTime, elapsedTime;
  }
  
  start() {
    this.startTime = new Date();
    this.inTime = new Date();
  }
  
  tour() { 
    this.inTime = new Date();
    this.elapsedTime = this.inTime - this.startTime; //in ms
    return this.elapsedTime;
  }
  
  end() {
    this.endTime = new Date();
    this.elapsedTime = this.endTime - this.startTime; //in ms
    //console.log(this.elapsedTime + "ms");
    // strip the ms
    return this.elapsedTime;
  }
}