class Basic_Rules {
  static chrono = new Chrono();
  
  constructor() {
  }

  start() {
    Basic_Rules.chrono.start();
    Events.on(engine, 'collisionStart', function(event) {
      var pairs = event.pairs;
      for (var i = 0, j = pairs.length; i < j; ++i) {
        if (pairs[i].bodyA.type == "Vehicle" && pairs[i].bodyB.type == "Gate")           {
          let gate = pairs[i].bodyB.gate;
          let vehicle = pairs[i].bodyA.vehicle;
          if ( vehicle.getTime() == -1 && vehicle.getToCheck() == gate.index){
            vehicle.setTime(Basic_Rules.chrono.end());
            gate.setColor(Math.random()*255, Math.random()*255, Math.random()*255);
            maps.restartEndChrono()
          }
          
        } else if (pairs[i].bodyB.type == "Vehicle" && pairs[i].bodyA.type == "Gate") {
          let gate = pairs[i].bodyA.gate;
          let vehicle = pairs[i].bodyB.vehicle;
          if ( vehicle.getTime() == -1 && vehicle.getToCheck() == gate.index){
            vehicle.setTime(Basic_Rules.chrono.end());
            gate.setColor(Math.random()*255, Math.random()*255, Math.random()*255);
            maps.restartEndChrono()
          } 
        }


        if (pairs[i].bodyA.type == "Vehicle" && pairs[i].bodyB.type == "KGate")           {
          let gate = pairs[i].bodyB.gate;
          let vehicle = pairs[i].bodyA.vehicle;
          vehicle.finish()
        } else if (pairs[i].bodyB.type == "Vehicle" && pairs[i].bodyA.type == "KGate") {
          let gate = pairs[i].bodyA.gate;
          let vehicle = pairs[i].bodyB.vehicle;
          vehicle.finish()
        } 
      }
    });
  }




}