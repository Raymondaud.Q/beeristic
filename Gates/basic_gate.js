class Basic_Gate{
  static num = -1;
  
   constructor(x, y, d){
    if (arguments.length == 3){
      this._body = Bodies.circle(x, y, d/2, {
        density: 1,
        friction: 1,
        frictionStatic: 5,
        diameter: d,
        isSensor: true,
        isStatic: true,
        collisionFilter: { category: gate }

      });
      World.add(world, this._body);
      this.noGravity();
      this._body.type="Gate";
      this._body.gate = this;
      this.index = ++Basic_Gate.num;
      this.color = color(255, 10, 10);
    }
  }


  setColor(colorr, colorg, colorb){
    this.color = color(colorr, colorg, colorb);
  }
  
  getPosition() {
    const pos = this._body.position;
    return { x:pos.x, y:pos.y, r:this._body.diameter, t:this._body.type };
  }


  remove(){
    World.remove(engine.world,this._body);
    delete this.noGravity;
    delete this._body;
    delete this.index ;
    delete this.color ;
  }

  show() {
    this.noGravity();
    const pos = this._body.position;
    const angle = this._body.angle;
    push();
    translate(pos.x, pos.y);
    rotate(angle);
    fill(this.color);
    circle(0, 0, this._body.diameter);
    imageMode(CENTER);
    image(gateImg, 0, 0, this._body.diameter, this._body.diameter);
    pop();
  }
  
  noGravity() {
    let gravity = engine.world.gravity;
    Matter.Body.applyForce(this._body, this._body.position, {
      x: -gravity.x * gravity.scale * this._body.mass,
      y: -gravity.y * gravity.scale * this._body.mass
    });
  }
  
}

class Collidable_Gate extends Basic_Gate{
  
   constructor(x, y, d){
    super();
    this._body = Bodies.circle(x, y, d/2, {
      density: 1,
      friction: 1,
      frictionStatic: 5,
      diameter: d,
      isStatic: true,
      collisionFilter: { category: cgate }
    });

    World.add(world, this._body);
    this.noGravity();
    this._body.type="CGate";
    this._body.gate = this;
    this.color = color(255, 255, 255);
  }

  show() {
    this.noGravity();
    const pos = this._body.position;
    const angle = this._body.angle;
    push();
    translate(pos.x, pos.y);
    fill(this.color);
    circle(0, 0, this._body.diameter);
    pop();
  }

}

class Kill_Gate extends Basic_Gate{
  
   constructor(x, y, d){
    super();
    this._body = Bodies.polygon(x, y, 6, d/2, {
      density: 1,
      friction: 1,
      frictionStatic: 5,
      diameter: d,
      isStatic: true,
      collisionFilter: { category: kgate }
    });

    World.add(world, this._body);
    this.noGravity();
    this._body.type="KGate";
    this._body.gate = this;
    this.color = color(255, 20, 0);
  }

  show() {
    this.noGravity();
    const pos = this._body.position;
    const angle = this._body.angle;
    push();
    translate(pos.x, pos.y);
    fill(this.color);
    hexagon(0, 0, 20);
    pop();
  }

}