class Gate_Manager{
 
  constructor(){
    this._gates = [];
  }

  
  clear(){
    for ( let gate of this._gates )
      gate.remove();
    this._gates = []
    Basic_Gate.num = -1
  }
  
  start(){
    for ( let gate of this._gates )
      gate.setColor(255, 0, 0);
  }
  
  size(){
    return this._gates.length;
  }
  
  
  add(gate){
    this._gates.push(gate);
  }
  
  getGates(){
    return this._gates;
  }
  
  getGatesPos(){
    let pos = [];
    for ( let gate  of this._gates)
      pos.push(gate.getPosition());
    return pos;
  }
  
  show(){
    for ( let gate of this._gates )
      gate.show();
  }
  
}